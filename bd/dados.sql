create database sigbar;
use sigbar;

create table Cliente(
	id int primary key auto_increment,
	nome varchar(50),
	cpf varchar(50),
	endereco varchar(50),
	telefone varchar(50),
	cartaoFidelidade varchar(50),
	credito varchar(50)
);


create table Garcon(
	id int primary key auto_increment,
	nome varchar(50),
	cpf varchar(50),
	endereco varchar(50),
	telefone varchar(50),
	matricula int,
	salario double,
	carteiraTrabalho varchar(50)
);

create table Mesa(
	id int primary key auto_increment,
	numero int
);

create table Item(
	id int primary key auto_increment,
	descricao varchar(100),
	valor double,
	marca varchar(50)
);


create table Conta(
	id int primary key auto_increment,
	garcon int,
	cliente int,
	mesa int,
	FOREIGN KEY (garcon) REFERENCES Garcon(id),
	FOREIGN KEY (cliente) REFERENCES Cliente(id),
	FOREIGN KEY (mesa) REFERENCES Mesa(id)
);

create table itemConta(
	id int primary key auto_increment,
	conta int,
	item int,
	quantidade int,
	FOREIGN KEY (conta) REFERENCES Conta(id),
	FOREIGN KEY (item) REFERENCES Item(id)
);