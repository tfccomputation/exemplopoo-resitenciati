/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import dao.MesaDAO;
import model.Mesa;

/**
 *
 * @author talisonfer
 */
public class MesaRepository extends AbstractRepository<Mesa> {
    public MesaRepository(){
        super(new MesaDAO());
    }
}
