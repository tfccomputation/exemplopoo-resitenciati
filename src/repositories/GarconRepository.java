/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import dao.GarconDAO;
import model.Garcon;

/**
 *
 * @author talisonfer
 */
public class GarconRepository extends AbstractRepository<Garcon>{
    public GarconRepository(){
        super(new GarconDAO());
    }
}
