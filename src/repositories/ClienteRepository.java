/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import dao.ClienteDAO;
import model.Cliente;

/**
 *
 * @author talisonfer
 */
public class ClienteRepository extends AbstractRepository<Cliente>{
    public ClienteRepository(){
        super(new ClienteDAO());
    }
}
