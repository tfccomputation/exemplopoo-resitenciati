/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import model.Conta;
import dao.ContaDAO;

/**
 *
 * @author talisonfer
 */
public class ContaRepository extends AbstractRepository<Conta>{
    public ContaRepository(){
        super(new ContaDAO());
    }
}
