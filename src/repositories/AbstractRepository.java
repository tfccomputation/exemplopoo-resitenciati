/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author talisonfer
 */
public class AbstractRepository<T> {
    
    private dao.DAO itens;

    public AbstractRepository(dao.DAO dao) {
        itens = dao;
    }
    
    public void addItem(T item){ 
        itens.add(item);
    }
    
    public void removeItem(T item) {
        itens.remove(item);
    }
      
    public List<T> listar(){
        return (List<T>)itens.list();
    }
}
