/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import dao.ItemContaDAO;
import model.ItemConta;

/**
 *
 * @author talisonfer
 */
public class ItemContaRepository extends AbstractRepository<ItemConta> {
    public ItemContaRepository(){
        super(new ItemContaDAO());
    }
}
