/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
 
import java.sql.DriverManager;
 
import java.sql.SQLException;

/**
 *
 * @author Gerdon
 */
public class ConnectionDB {
    private Connection con;
    
    public ConnectionDB(){
        this.con = null;
    }
    
    public Connection getConnection(){
        String serverName = "localhost";    //caminho do servidor do BD
        String mydatabase = "sigbar";        //nome do seu banco de dados
        String url = "jdbc:mysql://" + serverName + "/" + mydatabase;
        String username = "root";        //nome de um usuário de seu BD      
        String password = "";      //sua senha de acesso
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.con = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex){
            ex.printStackTrace();
        }
        
        return this.con;
    }
    
    public void close() throws SQLException{
        this.con.close();
    }
    
}
