/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Cliente;
import model.Conta;
import model.Garcon;
import model.Item;
import model.ItemConta;
import model.Mesa;

/**
 *
 * @author Gerdon
 */
public class ItemContaDAO implements DAO{
    private String table = "itemconta";
    
    @Override
    public boolean add(Object obj) {
        boolean resp = false;
        ItemConta itemConta = (ItemConta)obj;
        Connection con = new ConnectionDB().getConnection();
        String sql = "insert into "+table+" values (null,?,?,?)";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, itemConta.getConta().getId());
            pstm.setInt(2, itemConta.getItem().getId());
            pstm.setInt(3, itemConta.getQuantidade());
            resp = pstm.execute();
            
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resp;
    }

    @Override
    public boolean remove(Object obj) {
        boolean resp = false;
        ItemConta itemConta = (ItemConta)obj;
        Connection con = new ConnectionDB().getConnection();
        String sql = "delete from "+table+" where id=?";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, itemConta.getId());
            resp = pstm.execute();
            
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return resp;
    }

    @Override
    public Object list() {
        List<ItemConta> lista = new ArrayList<ItemConta>();
        PreparedStatement pstm = null;
        Connection con = new ConnectionDB().getConnection();
        
        String sql = "select * from "+table;
        try {
            pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            
            while(rs.next()){
                ItemConta itemConta = new ItemConta();
                itemConta.setId(rs.getInt("id"));
                
                Conta conta = new Conta();
                conta.setId(rs.getInt("conta"));
                
                Item item = new Item();
                item.setId(rs.getInt("item"));
                
                itemConta.setConta(conta);
                itemConta.setItem(item);
                itemConta.setQuantidade(rs.getInt("quantidade"));
                
                lista.add(itemConta);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        try {
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return lista;
    }
}
