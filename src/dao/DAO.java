/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Gerdon
 */
public interface DAO {
    public boolean add(Object obj);
    public boolean remove(Object obj);
    public Object list();
}
