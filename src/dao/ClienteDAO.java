/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Cliente;

/**
 *
 * @author Gerdon
 */
public class ClienteDAO implements DAO{

    @Override
    public boolean add(Object obj) {
        boolean resp = false;
        Cliente cliente = (Cliente)obj;
        Connection con = new ConnectionDB().getConnection();
        String sql = "insert into cliente values (null,?,?,?,?,?,?)";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, cliente.getNome());
            pstm.setString(2, cliente.getCpf());
            pstm.setString(3, cliente.getEndereco());
            pstm.setString(4, cliente.getTelefone());
            pstm.setString(5, cliente.getCartaoFidelidade());
            pstm.setString(6, cliente.getCredito());
            resp = pstm.execute();
            
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resp;
    }

    @Override
    public boolean remove(Object obj) {
        boolean resp = false;
        Cliente cliente = (Cliente)obj;
        Connection con = new ConnectionDB().getConnection();
        String sql = "delete from cliente where id=?";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, cliente.getId());
            resp = pstm.execute();
            
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return resp;
    }
    
    
    @Override
    public List<Cliente> list() {
        List<Cliente> lista = new ArrayList<Cliente>();
        PreparedStatement pstm = null;
        Connection con = new ConnectionDB().getConnection();
        
        String sql = "select * from cliente";
        try {
            pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            
            while(rs.next()){
                Cliente temp = new Cliente();
                temp.setId(rs.getInt("id"));
                temp.setNome(rs.getString("nome"));
                temp.setCpf(rs.getString("cpf"));
                temp.setEndereco(rs.getString("endereco"));
                temp.setTelefone(rs.getString("telefone"));
                temp.setCartaoFidelidade(rs.getString("cartaoFidelidade"));
                temp.setCredito(rs.getString("credito"));
                lista.add(temp);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        try {
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return lista;
    }
    
}
