/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Garcon;

/**
 *
 * @author Gerdon
 */
public class GarconDAO implements DAO{

    @Override
    public boolean add(Object obj) {
        boolean resp = false;
        Garcon garcon = (Garcon)obj;
        Connection con = new ConnectionDB().getConnection();
        String sql = "insert into garcon values (null,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, garcon.getNome());
            pstm.setString(2, garcon.getCpf());
            pstm.setString(3, garcon.getEndereco());
            pstm.setString(4, garcon.getTelefone());
            pstm.setInt(5, Integer.parseInt(garcon.getMatricula()));
            pstm.setDouble(6, garcon.getSalario());
            pstm.setString(7, garcon.getCarteiraTrabalho());
            resp = pstm.execute();
            
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resp;
    }

    @Override
    public boolean remove(Object obj) {
        boolean resp = false;
        Garcon garcon = (Garcon)obj;
        Connection con = new ConnectionDB().getConnection();
        String sql = "delete from garcon where id=?";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, garcon.getId());
            resp = pstm.execute();
            
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return resp;
    }

    @Override
    public Object list() {
        List<Garcon> lista = new ArrayList<Garcon>();
        PreparedStatement pstm = null;
        Connection con = new ConnectionDB().getConnection();
        
        String sql = "select * from garcon";
        try {
            pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            
            while(rs.next()){
                Garcon garcon = new Garcon();
                garcon.setId(rs.getInt("id"));
                garcon.setNome(rs.getString("nome"));
                garcon.setCpf(rs.getString("cpf"));
                garcon.setEndereco(rs.getString("endereco"));
                garcon.setTelefone(rs.getString("telefone"));
                garcon.setMatricula(""+rs.getInt("matricula"));
                garcon.setSalario(rs.getDouble("salario"));
                garcon.setCarteiraTrabalho(rs.getString("carteiraTrabalho"));
                lista.add(garcon);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        try {
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return lista;
    }
    
}
