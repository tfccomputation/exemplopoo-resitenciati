/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Item;

/**
 *
 * @author Gerdon
 */
public class ItemDAO implements DAO{
    
    private String table = "item";
    
    @Override
    public boolean add(Object obj) {
        boolean resp = false;
        Item item = (Item)obj;
        Connection con = new ConnectionDB().getConnection();
        String sql = "insert into "+table+" values (null,?,?,?)";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, item.getDescricao());
            pstm.setDouble(2, item.getValor());
            pstm.setString(3, item.getMarca());
            resp = pstm.execute();
            
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resp;
    }

    @Override
    public boolean remove(Object obj) {
        boolean resp = false;
        Item item = (Item)obj;
        Connection con = new ConnectionDB().getConnection();
        String sql = "delete from "+table+" where id=?";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, item.getId());
            resp = pstm.execute();
            
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return resp;
    }

    @Override
    public Object list() {
        List<Item> lista = new ArrayList<Item>();
        PreparedStatement pstm = null;
        Connection con = new ConnectionDB().getConnection();
        
        String sql = "select * from "+table;
        try {
            pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            
            while(rs.next()){
                Item item = new Item();
                item.setId(rs.getInt("id"));
                item.setDescricao(rs.getString("descricao"));
                item.setValor(rs.getDouble("valor"));
                item.setMarca(rs.getString("marca"));
                lista.add(item);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        try {
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return lista;
    }
}
