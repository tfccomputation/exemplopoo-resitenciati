/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Mesa;

/**
 *
 * @author Gerdon
 */
public class MesaDAO implements DAO{
    
    private String table = "mesa";
    
    @Override
    public boolean add(Object obj) {
        boolean resp = false;
        Mesa mesa = (Mesa)obj;
        Connection con = new ConnectionDB().getConnection();
        String sql = "insert into "+table+" values (null,?)";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, mesa.getNumero());
            resp = pstm.execute();
            
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resp;
    }

    @Override
    public boolean remove(Object obj) {
        boolean resp = false;
        Mesa mesa = (Mesa)obj;
        Connection con = new ConnectionDB().getConnection();
        String sql = "delete from "+table+" where id=?";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, mesa.getId());
            resp = pstm.execute();
            
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return resp;
    }

    @Override
    public Object list() {
        List<Mesa> lista = new ArrayList<Mesa>();
        PreparedStatement pstm = null;
        Connection con = new ConnectionDB().getConnection();
        
        String sql = "select * from "+table;
        try {
            pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            
            while(rs.next()){
                Mesa mesa = new Mesa();
                mesa.setId(rs.getInt("id"));
                mesa.setNumero(rs.getInt("numero"));
                lista.add(mesa);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        try {
            pstm.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return lista;
    }
}
