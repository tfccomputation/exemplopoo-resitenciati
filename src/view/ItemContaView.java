/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ItemContaController;
import repositories.ItemContaRepository;

/**
 *
 * @author alisson nascimento
 */
public class ItemContaView extends AbstractView<ItemContaController>{
    
    public ItemContaView() {
        super(new ItemContaController(new ItemContaRepository()));
    }
    
}
