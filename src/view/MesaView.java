/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.MesaController;
import repositories.MesaRepository;

/**
 *
 * @author alisson nascimento
 */
public class MesaView extends AbstractView<MesaController>{
    
    public MesaView() {
        super(new MesaController(new MesaRepository()));
    }
    
}
