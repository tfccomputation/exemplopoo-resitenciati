/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

/**
 * 
 * @author alisson nascimento
 * @param <C> 
 */
public class AbstractView<C> {
    private C controller;
    
    public AbstractView(C controller){
        //this.repository = repositorio;
        this.controller = controller;
    }
    
    
    public C getController() {
        return controller;
    }

    public void setController(C controller) {
        this.controller = controller;
    }
    
}
