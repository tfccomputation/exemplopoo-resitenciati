/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ContaController;
import controller.ItemContaController;
import controller.ItemController;
import javax.swing.JOptionPane;
import model.Item;
import repositories.ContaRepository;
import repositories.ItemContaRepository;
import repositories.ItemRepository;

/**
 *
 * @author alisson nascimento
 */
public class ContaView extends AbstractView<ContaController>{
    
    public ContaView() {
        super(new ContaController(new ContaRepository()));
    }
    
}
