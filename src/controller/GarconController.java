/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Garcon;
import repositories.GarconRepository;

/**
 *
 * @author talisonfer
 */
public class GarconController extends AbstractyController<GarconRepository, Garcon> {
    
    public GarconController(GarconRepository garconRepository){
        super(garconRepository);
    }
    
    
    @Override
    public void salvar(Garcon item) {
        getRepository().addItem(item);
    }
    
    @Override
    public void remover(Garcon item) {
        getRepository().removeItem(item);
    }
    
    @Override
    public void atualizar(Garcon itemOld, Garcon itemNew){
        getRepository().removeItem(itemOld);
        getRepository().addItem(itemNew);
    }

    @Override
    String listar() {
        String lista = "Lista de clientes \n";
        for (Garcon item : getRepository().listar()) {
            lista += item + "\n";
        }
        return lista;
    }
}
