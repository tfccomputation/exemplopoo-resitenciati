/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.Item;
import repositories.ItemRepository;

/**
 *
 * @author itamir.filho
 */
public class ItemController extends AbstractyController<ItemRepository, Item>{
    
    public ItemController(ItemRepository itemRepository) {
        super(itemRepository);
    }
    
    @Override
    public void salvar(Item item) {
        getRepository().addItem(item);
    }
    
    @Override
    public void remover(Item item) {
        getRepository().removeItem(item);
    }
    
    @Override
    public void atualizar(Item itemOld, Item itemNew){
        getRepository().removeItem(itemOld);
        getRepository().addItem(itemNew);
    }
    
    @Override
    public String listar() {
        String lista = "Lista de itens \n";
        for (Item item : getRepository().listar()) {
            lista += item + "\n";
        }
        return lista;
    }
}
