/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Conta;
import repositories.ContaRepository;

/**
 *
 * @author talisonfer
 */
public class ContaController extends AbstractyController<ContaRepository, Conta>{
    public ContaController(ContaRepository contaRepository){
        super(contaRepository);
    }
    
    @Override
    public void salvar(Conta item) {
        getRepository().addItem(item);
    }
    
    @Override
    public void remover(Conta item) {
        getRepository().removeItem(item);
    }
    
    @Override
    public void atualizar(Conta itemOld, Conta itemNew){
        getRepository().removeItem(itemOld);
        getRepository().addItem(itemNew);
    }

    @Override
    String listar() {
        String lista = "Lista de contas \n";
        for (Conta item : getRepository().listar()) {
            lista += item + "\n";
        }
        return lista;
    }
}
