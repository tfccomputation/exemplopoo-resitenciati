/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.ItemConta;
import repositories.ItemContaRepository;

/**
 *
 * @author talisonfer
 */
public class ItemContaController extends AbstractyController<ItemContaRepository, ItemConta>{
    
    public ItemContaController(ItemContaRepository  itemContaRepository){
        super(itemContaRepository);
    }
    
    
    @Override
    public void salvar(ItemConta item) {
        getRepository().addItem(item);
    }
    
    @Override
    public void remover(ItemConta item) {
        getRepository().removeItem(item);
    }
    
    @Override
    public void atualizar(ItemConta itemOld, ItemConta itemNew){
        getRepository().removeItem(itemOld);
        getRepository().addItem(itemNew);
    }

    @Override
    String listar() {
        String lista = "Lista de clientes \n";
        for (ItemConta item : getRepository().listar()) {
            lista += item + "\n";
        }
        return lista;
    }
}
