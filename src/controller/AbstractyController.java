/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import repositories.AbstractRepository;

/**
 *
 * @author talisonfer
 * @author alisson nascimento
 */
abstract class AbstractyController<T,K>{
    
    private T repository;
    
    public AbstractyController(T repository){
        this.repository = repository;
    }
    
    abstract void salvar(K item);
    
    abstract void remover(K item);
    
    abstract void atualizar(K itemOld, K itemNew);
    
    abstract String listar();
    
    public T getRepository() {
        return repository;
    }
}
