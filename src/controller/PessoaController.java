/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Pessoa;
import repositories.PessoaRepository;

/**
 *
 * @author talisonfer
 */
public class PessoaController extends AbstractyController<PessoaRepository, Pessoa>{
    
    public PessoaController(PessoaRepository pessoaRepository){
        super(pessoaRepository);
    }
    
    @Override
    public void salvar(Pessoa item) {
        getRepository().addItem(item);
    }
    
    @Override
    public void remover(Pessoa item) {
        getRepository().removeItem(item);
    }
    
    @Override
    public void atualizar(Pessoa itemOld, Pessoa itemNew){
        getRepository().removeItem(itemOld);
        getRepository().addItem(itemNew);
    }

    @Override
    String listar() {
        String lista = "Lista de clientes \n";
        for (Pessoa item : getRepository().listar()) {
            lista += item + "\n";
        }
        return lista;
    }
}
