/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Mesa;
import repositories.MesaRepository;

/**
 *
 * @author talisonfer
 */
public class MesaController extends AbstractyController<MesaRepository, Mesa>{
    
    public MesaController(MesaRepository mesaRepository){
        super(mesaRepository);
    }
    
    @Override
    public void salvar(Mesa item) {
        getRepository().addItem(item);
    }
    
    @Override
    public void remover(Mesa item) {
        getRepository().removeItem(item);
    }
    
    @Override
    public void atualizar(Mesa itemOld, Mesa itemNew){
        getRepository().removeItem(itemOld);
        getRepository().addItem(itemNew);
    }

    @Override
    String listar() {
        String lista = "Lista de clientes \n";
        for (Mesa item : getRepository().listar()) {
            lista += item + "\n";
        }
        return lista;
    }
}
