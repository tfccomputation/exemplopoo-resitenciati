/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Cliente;
import repositories.ClienteRepository;

/**
 *
 * @author talisonfer
 */
public class ClienteController extends AbstractyController<ClienteRepository, Cliente> {
    
    public ClienteController(ClienteRepository clienteRepositprio){
        super(clienteRepositprio);
    }

    @Override
    public void salvar(Cliente item) {
        getRepository().addItem(item);
    }
    
    @Override
    public void remover(Cliente item) {
        getRepository().removeItem(item);
    }
    
    @Override
    public void atualizar(Cliente itemOld, Cliente itemNew){
        getRepository().removeItem(itemOld);
        getRepository().addItem(itemNew);
    }

    @Override
    String listar() {
        String lista = "Lista de clientes \n";
        for (Cliente item : getRepository().listar()) {
            lista += item + "\n";
        }
        return lista;
    }
}
